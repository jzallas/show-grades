package com.sample.two;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;

/**
 *	A Student with a name, grade, and a thumbnail
 */
public class Student implements Parcelable {
	private String name;
	private float grade;
	private Uri thumbnailUri;

	/**
	 * Creates a student with a name, a grade, and a thumbnail
	 * 
	 * @param name
	 *            - a name to set for the student
	 * @param grade
	 *            - a grade to set for the student
	 * @param thumbnailUrl
	 *            - a Uri containing a url to the thumbnail for the student
	 */
	public Student(String name, float grade, Uri thumbnailUrl) {
		this.name = name;
		this.grade = grade;
		this.thumbnailUri = thumbnailUrl;
	}

	/**
	 * @return the name of this student
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            - a name to set for the student
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the grade of this student
	 */
	public float getGrade() {
		return grade;
	}
	
	/**
	 * @return the grade of this student formatted as a String
	 */
	public String getFormattedGrade(){
		NumberFormat formatter = new DecimalFormat();
		return formatter.format(grade);
	}

	/**
	 * @param grade
	 *            - a grade to set for this student
	 */
	public void setGrade(float grade) {
		this.grade = grade;
	}

	/**
	 * @return the thumbnail Uri of this student
	 */
	public Uri getThumbnailUri() {
		return thumbnailUri;
	}

	/**
	 * @param thumbnail
	 *            - a Uri containing a link to the thumbnail for this student
	 */
	public void setThumbnailUri(Uri thumbnailUri) {
		this.thumbnailUri = thumbnailUri;
	}

	/**
	 * @return - a bitmap containing the thumbnail image
	 */
	public void getThumbnailImage(ImageView v) {
		// run asynctask to check db for image first, if not in db, download and
		// put in db
	}

	// **********PARCELABLE METHODS*************//

	@Override
	public int describeContents() {
		// this method is irrelevant to the use of this object
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeFloat(grade);
		dest.writeParcelable(thumbnailUri, 0);
	}

	public static final Parcelable.Creator<Student> CREATOR = new Creator<Student>() {
		public Student createFromParcel(Parcel source) {
			return new Student(source.readString(), source.readFloat(),
					(Uri) source.readParcelable(null));
		}

		public Student[] newArray(int size) {
			return new Student[size];
		}
	};

}
