package com.sample.two;

import java.util.List;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ShowGradesActivity extends ListActivity {

	List<Student> students;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		students = getIntent().getParcelableArrayListExtra(
				MainActivity.DOWNLOADED_STUDENT_LIST);
		getListView().setAdapter(new StudentAdapter(students));
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setBackgroundDrawable(
					new ColorDrawable(getResources().getColor(
							R.color.action_bar_color)));
		}
	}

	private class StudentAdapter extends BaseAdapter {

		private List<Student> students;
		
		/**
		 * Create an adapter based off of a list of {@link Student} objects
		 * @param students
		 */
		public StudentAdapter(List<Student> students){
			this.students = students;
		}

		@Override
		public int getCount() {
			if (students == null)
				return 0;
			return students.size();
		}

		@Override
		public Object getItem(int position) {
			if (students == null)
				return null;
			return students.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			RelativeLayout studentView;
			if (convertView == null)
				studentView = (RelativeLayout) getLayoutInflater().inflate(
						R.layout.student_list_item, null);
			else
				studentView = (RelativeLayout) convertView;

			// get views to set data
			ImageView thumbnail = (ImageView) studentView
					.findViewById(R.id.thumbnail_imageView);
			TextView name = (TextView) studentView
					.findViewById(R.id.name_textView);
			TextView grade = (TextView) studentView
					.findViewById(R.id.grade_textView);

			// if this view is recycled, remove the previous image before trying
			// to get the correct image
			thumbnail.setImageBitmap(null);

			Student student = students.get(position);

			// look for image asynchronously
			new LoadImageTask(getApplicationContext(), thumbnail)
					.execute(new String[] { student.getThumbnailUri()
							.toString() });

			// set the name and grade
			name.setText(student.getName());
			grade.setText(student.getFormattedGrade());

			return studentView;
		}
	};

}
