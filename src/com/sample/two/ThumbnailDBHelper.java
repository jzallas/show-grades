package com.sample.two;

import java.io.ByteArrayOutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class ThumbnailDBHelper extends SQLiteOpenHelper {

	// database version
	private static final int DATABASE_VERSION = 1;

	// database name
	private static final String DATABASE_NAME = "thumbnail_db";

	// table names
	private static final String TABLE_THUMBS = "thumbnails";

	// shared column names
	private static final String KEY_ID = "_id";

	// thumbnails column names
	private static final String KEY_THUMBNAIL_URL = "url";
	private static final String KEY_THUMBNAIL_IMAGE = "image";

	// table create statement
	private static final String CREATE_TABLE_THUMBS = "CREATE TABLE "
			+ TABLE_THUMBS + "(" + KEY_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_THUMBNAIL_URL
			+ " TEXT UNIQUE," + KEY_THUMBNAIL_IMAGE + " BLOB)";

	public ThumbnailDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_THUMBS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_THUMBS);
		onCreate(db);
	}


	/**
	 * insert a thumbnail into the database
	 * 
	 * @param thumbnailURL
	 *            - the url where the thumbnail was loaded from
	 * @param thumbnail
	 *            - a bitmap containing the thumbnail
	 * @return - true if insert was successful, false otherwise
	 */
	public boolean insert(String thumbnailURL, Bitmap thumbnail) {
		SQLiteDatabase db = getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(KEY_THUMBNAIL_IMAGE, BitmapToByteArray(thumbnail));
		values.put(KEY_THUMBNAIL_URL, thumbnailURL);
		try {
			db.insertOrThrow(TABLE_THUMBS, null, values);
		} catch (SQLException sqlException) {
			return false;
		}
		return true;

	}

	private byte[] BitmapToByteArray(Bitmap bmp) {
		if (bmp != null) {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
			byte[] byteArray = stream.toByteArray();
			return byteArray;
		}
		return null;
	}

	private Bitmap ByteArrayToBitmap(byte[] src) {
		return BitmapFactory.decodeByteArray(src, 0, src.length);
	}

	/**
	 * Gets a thumbnail from the database
	 * @param thumbnailURL
	 *            - the url where the thumbnail was loaded from
	 * @return - a bitmap containing the thumbnail if it was in the database.
	 *         <em>null</em> will be returned if the thumbnail was not in the
	 *         database
	 */
	public Bitmap getThumbnail(String thumbnailURL) {
		SQLiteDatabase db = getReadableDatabase();

		Cursor c = db.query(TABLE_THUMBS, new String[] { KEY_THUMBNAIL_IMAGE },
				KEY_THUMBNAIL_URL + "=?", new String[] { thumbnailURL }, null,
				null, null);

		Bitmap thumb = null;
		if (c.moveToFirst()) {
			thumb = ByteArrayToBitmap(c.getBlob(c
					.getColumnIndex(KEY_THUMBNAIL_IMAGE)));
			c.close();
		}
		return thumb;
	}

	/**
	 * closes the database after opening it
	 */
	public void closeDB() {
		SQLiteDatabase db = this.getReadableDatabase();
		if (db != null && db.isOpen())
			db.close();
	}
}
