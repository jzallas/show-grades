package com.sample.two;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

public class HttpGETAsyncTask extends AsyncTask<Void, Void, List<Student>> {

	private static final String gradesUrl = "http://www.fps.net63.net/challenge_grades.json";

	private AndroidHttpClient httpClient = AndroidHttpClient.newInstance("");
	
	private ListView listView;
	private ProgressBar progressBar;
	private View footerView;
	
	public HttpGETAsyncTask(ProgressBar progressBar, ListView listView, View footerView){
		this.progressBar = progressBar;
		this.listView = listView;
		this.footerView = footerView;
	}
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		progressBar.setVisibility(View.VISIBLE);
	}

	@Override
	protected List<Student> doInBackground(Void... params) {

		HttpGet request = new HttpGet(gradesUrl);

		JSONResponseHandler responseHandler = new JSONResponseHandler();

		try {
			return httpClient.execute(request, responseHandler);
		} catch (ClientProtocolException e) {
			// catch error in http protocol
		} catch (IOException e) {
			// catch generic connection issue
		} finally {
			if (httpClient != null)
				httpClient.close();
		}
		

		return null;
	}

	@Override
	protected void onPostExecute(List<Student> result) {
		progressBar.setVisibility(View.GONE);
		listView.addFooterView(footerView);

	}
}
