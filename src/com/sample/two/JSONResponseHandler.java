package com.sample.two;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.net.Uri;

public class JSONResponseHandler implements ResponseHandler<List<Student>> {

	private static String TAG_GRADES = "grades";

	private static String TAG_GRADE = "grade";
	private static String TAG_STUDENT = "student";
	private static String TAG_THUMBNAIL = "thumbnail";

	@Override
	public List<Student> handleResponse(HttpResponse response)
			throws ClientProtocolException, IOException {

		String JSONResponse = new BasicResponseHandler()
				.handleResponse(response);
		List<Student> students = new ArrayList<Student>();
		try {
			JSONArray result = new JSONObject(JSONResponse)
					.getJSONArray(TAG_GRADES);
			for (int i = 0; i < result.length(); i++) {
				JSONObject thisStudent = (JSONObject) result.get(i);
				addStudent(students, thisStudent);
			}

		} catch (JSONException e) {
			// if the parse fails or a JSON Object couldn't be created
		}
		return students;
	}

	/**
	 * parses the student then adds the parsed student to the list. Duplicate additions are ignored.
	 * @param students - the list containing {@link Student}s to add to
	 * @param jsonStudent - a json object representing the student
	 * @throws JSONException - if the parse fails or a json object couldn't be created
	 */
	private void addStudent(List<Student> students, JSONObject jsonStudent)
			throws JSONException {

		String name = jsonStudent.getString(TAG_STUDENT);
		if (!exists(students, name)) { //check to see if this is a duplicate
			float grade = (float) jsonStudent.getDouble(TAG_GRADE);
			Uri thumbnail = Uri.parse(jsonStudent.getString(TAG_THUMBNAIL));
			students.add(new Student(name, grade, thumbnail));
		}

	}

	/**
	 * Checks to see if a student already exists
	 * 
	 * @param students
	 *            - the list of students to check
	 * @param name
	 *            - the name to look for
	 * @return true if the name is in the list already, false if it is not
	 */
	private boolean exists(List<Student> students, String name) {
		for (Student s : students) {
			if (s.getName().equals(name))
				return true;
		}
		return false;
	}
}
