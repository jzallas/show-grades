package com.sample.two;

import java.io.InputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

/**
 * Loads a thumbnail for this application asynchronously. First, the task checks
 * a database to see if the thumbnail has been cached. If the thumbnail is not
 * in the database, the task will download the thumbnail, store it in the
 * database, and update the provided ImageView with the thumbnail when it is
 * available
 */
class LoadImageTask extends AsyncTask<String, Void, Bitmap> {
	ImageView imageView;
	Context context;

	public LoadImageTask(Context context, ImageView imageView) {
		this.context = context;
		this.imageView = imageView;
	}

	protected Bitmap doInBackground(String... urls) {
		Bitmap thumbnail = null;
		String thumbnailURL = urls[0];

		ThumbnailDBHelper db = new ThumbnailDBHelper(context);
		thumbnail = db.getThumbnail(thumbnailURL);

		// if it wasn't in the database then download it
		if (thumbnail == null) {
			try {
				InputStream in = new java.net.URL(thumbnailURL).openStream();
				thumbnail = BitmapFactory.decodeStream(in);
				// insert into database for future
				db.insert(thumbnailURL, thumbnail);
			} catch (Exception e) {
			}

		}
		db.closeDB();
		return thumbnail;
	}

	protected void onPostExecute(Bitmap result) {
		imageView.setImageBitmap(result);
	}
}