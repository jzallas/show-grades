package com.sample.two;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

// Note: This implementation specifically asked for a listview on the first screen 
// but if the adapter is never used it defeats the purpose of the listview and the
// same effect could be generated using better methods
public class MainActivity extends ListActivity implements View.OnClickListener {

	public final static String DOWNLOADED_STUDENT_LIST = "MainActivity.StudentList";

	private ProgressBar spinner;
	private View headerView, footerView;
	private HttpGETAsyncTask loadTask;

	@SuppressWarnings("rawtypes")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		createHeaderAndFooter();
		getListView().addHeaderView(headerView);

		// need to add an empty adapter in order to get the listview to show
		// the header and footer views
		getListView().setAdapter(
				new ArrayAdapter(this, android.R.layout.simple_list_item_1));

		// Show the Up button in the action bar.
		setupActionBar();

	}

	private void createHeaderAndFooter() {
		headerView = getLayoutInflater().inflate(R.layout.list_header, null);
		Button load = (Button) headerView.findViewById(R.id.load_button);
		load.setOnClickListener(this);
		spinner = (ProgressBar) headerView.findViewById(R.id.progressBar1);
		footerView = getLayoutInflater().inflate(R.layout.list_footer, null);
		Button show = (Button) footerView.findViewById(R.id.show_button);
		show.setOnClickListener(this);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setBackgroundDrawable(
					new ColorDrawable(getResources().getColor(
							R.color.action_bar_color)));
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.load_button:
			if (isNetworkConnected()) {
				loadTask = (HttpGETAsyncTask) new HttpGETAsyncTask(spinner,
						getListView(), footerView).execute();
				// if this button was pressed a second time, disable the show
				// grades view until the new download is complete
				getListView().removeFooterView(footerView);
			}
			else
				Toast.makeText(this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
			break;
		case R.id.show_button:
			Intent i = new Intent(this, ShowGradesActivity.class);
			try {
				i.putParcelableArrayListExtra(DOWNLOADED_STUDENT_LIST,
						(ArrayList<Student>) loadTask.get());
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			startActivity(i);
			break;
		default:
			break;
		}

	}

	/**
	 * @return true if there is a network connection; false if there was a
	 *         problem or not connected
	 */
	private boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			// There are no active networks.
			return false;
		} else
			return true;
	}
}
